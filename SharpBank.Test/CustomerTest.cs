﻿using NUnit.Framework;
using SharpBank.Services;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {

            AccountService checkingAccount = new AccountService(AccountService.CHECKING);
            AccountService savingsAccount = new AccountService(AccountService.SAVINGS);

            CustomerService henry = new CustomerService("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
            CustomerService oscar = new CustomerService("Oscar").OpenAccount(new AccountService(AccountService.SAVINGS));
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            CustomerService oscar = new CustomerService("Oscar")
                    .OpenAccount(new AccountService(AccountService.SAVINGS));
            oscar.OpenAccount(new AccountService(AccountService.CHECKING));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAccounts()
        {
            CustomerService oscar = new CustomerService("Oscar")
                    .OpenAccount(new AccountService(AccountService.SAVINGS));
            oscar.OpenAccount(new AccountService(AccountService.CHECKING));
            oscar.OpenAccount(new AccountService(AccountService.MAXI_SAVINGS));
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTransferBetweenAccounts()
        {
            AccountService checkingAccount = new AccountService(AccountService.CHECKING);
            AccountService savingsAccount = new AccountService(AccountService.SAVINGS);

            CustomerService customer = new CustomerService("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(2000.0);

            Assert.AreEqual("Operation completed successfully", customer.TransferBetweenAccounts(1000.0, checkingAccount, savingsAccount));
            Assert.AreEqual(1000, checkingAccount.SumTransactions());
            Assert.AreEqual(1000, savingsAccount.SumTransactions());
        }

        [Test]
        public void TestTransferBetweenAccountsFail()
        {
            AccountService checkingAccount = new AccountService(AccountService.CHECKING);
            AccountService savingsAccount = new AccountService(AccountService.SAVINGS);

            CustomerService customer = new CustomerService("Henry").OpenAccount(checkingAccount);

            checkingAccount.Deposit(2000.0);

            Assert.AreEqual("Operation Failed", customer.TransferBetweenAccounts(1000.0, checkingAccount, savingsAccount));
        }
    }
}
