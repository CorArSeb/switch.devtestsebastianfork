﻿using NUnit.Framework;
using SharpBank.Services;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            BankService bank = new BankService();
            CustomerService john = new CustomerService("John");
            john.OpenAccount(new AccountService(AccountService.CHECKING));
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            BankService bank = new BankService();
            AccountService checkingAccount = new AccountService(AccountService.CHECKING);
            CustomerService bill = new CustomerService("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            BankService bank = new BankService();
            AccountService checkingAccount = new AccountService(AccountService.SAVINGS);
            bank.AddCustomer(new CustomerService("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(1500.0);

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            BankService bank = new BankService();
            AccountService checkingAccount = new AccountService(AccountService.MAXI_SAVINGS);
            bank.AddCustomer(new CustomerService("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(3000.0);

            Assert.AreEqual(150.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

    }
}
