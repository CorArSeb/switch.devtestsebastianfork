﻿using System;
using System.Collections.Generic;

namespace SharpBank.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly string name;
        private List<AccountService> accounts;

        public CustomerService(string name)
        {
            this.name = name;
            accounts = new List<AccountService>();
        }

        public string GetName()
        {
            return name;
        }

        public CustomerService OpenAccount(AccountService account)
        {
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double GetTotalInterestEarned()
        {
            double total = 0.0;
            foreach (AccountService account in accounts)
                total += account.InterestEarned();
            return total;
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public string GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            string statement = null; //reset statement to null here
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end
            statement = "Statement for " + name + "\n";
            double total = 0.0;

            foreach (AccountService account in accounts)
            {
                statement += "\n" + StatementForAccount(account) + "\n";
                total += account.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        private string StatementForAccount(AccountService account)
        {
            string accountStatement = "";

            //Translate to pretty account type
            switch (account.GetAccountType())
            {
                case AccountService.CHECKING:
                    accountStatement += "Checking Account\n";
                    break;
                case AccountService.SAVINGS:
                    accountStatement += "Savings Account\n";
                    break;
                case AccountService.MAXI_SAVINGS:
                    accountStatement += "Maxi Savings Account\n";
                    break;
            }

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction transaction in account.transactions)
            {
                accountStatement += "  " + (transaction.amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(transaction.amount) + "\n";
                total += transaction.amount;
            }
            accountStatement += "Total " + ToDollars(total);
            return accountStatement;
        }

        private string ToDollars(double d)
        {
            return string.Format("${0:N2}", Math.Abs(d));
        }

        public string TransferBetweenAccounts(double amount, AccountService accountFrom, AccountService accountTo)
        {
            if (accounts.Contains(accountFrom) && accounts.Contains(accountTo))
            {
                accountFrom.Withdraw(amount);
                accountTo.Deposit(amount);

                return "Operation completed successfully";
            }
            return "Operation Failed";
        }
    }
}
