﻿namespace SharpBank.Services
{
    public interface IAccountService
    {
        void Deposit(double amount);
        int GetAccountType();
        double InterestEarned();
        double InterestEarnedDaily();
        double SumTransactions();
        void Withdraw(double amount);
    }
}