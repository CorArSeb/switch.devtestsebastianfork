﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpBank.Services
{
    public class AccountService : IAccountService
    {

        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;

        private readonly int accountType;
        public List<Transaction> transactions;

        public AccountService(int accountType)
        {
            this.accountType = accountType;
            transactions = new List<Transaction>();
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(-amount));
            }
        }

        public double InterestEarned()
        {
            double amount = SumTransactions();

            switch (accountType)
            {
                case SAVINGS:
                    if (amount <= 1000)
                        return amount * 0.001;
                    else
                        return 1 + (amount - 1000) * 0.002;
                case MAXI_SAVINGS:
                    var lastTransactionDate = transactions.LastOrDefault().transactionDate;
                    if (lastTransactionDate.AddDays(10) < DateProvider.GetInstance().Now())
                        return amount * 0.001;
                    else
                        return amount * 0.05;
                default:
                    return amount * 0.001;
            }
        }

        public double InterestEarnedDaily() //no tendria claro el valor diario de intereses, necesitaria clarificar la formula diaria
        {
            double amount = SumTransactions();

            switch (accountType)
            {
                case SAVINGS:
                    if (amount <= 1000)
                        return amount * (0.001 / 365);
                    else
                        return 1 + (amount - 1000) * (0.002 / 365);
                case MAXI_SAVINGS:
                    var lastTransactionDate = transactions.LastOrDefault().transactionDate;
                    if (lastTransactionDate.AddDays(10) < DateProvider.GetInstance().Now())
                        return amount * (0.001 / 365);
                    else
                        return amount * (0.05 / 365);
                default:
                    return amount * (0.001 / 365);
            }
        }

        public double SumTransactions()
        {
            double amount = 0.0;
            if (transactions.Any())
            {
                foreach (Transaction t in transactions)
                    amount += t.amount;

            }
            return amount;
        }

        public int GetAccountType()
        {
            return accountType;
        }

    }
}
