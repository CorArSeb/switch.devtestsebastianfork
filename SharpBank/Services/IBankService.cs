﻿namespace SharpBank.Services
{
    public interface IBankService
    {
        void AddCustomer(CustomerService customer);
        string CustomerSummary();
        string GetFirstCustomer();
        double TotalInterestPaid();
    }
}