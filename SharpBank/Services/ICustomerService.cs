﻿namespace SharpBank.Services
{
    public interface ICustomerService
    {
        string GetName();
        int GetNumberOfAccounts();
        string GetStatement();
        double GetTotalInterestEarned();
        CustomerService OpenAccount(AccountService account);
        string TransferBetweenAccounts(double amount, AccountService accountFrom, AccountService accountTo);
    }
}