﻿using System;
using System.Collections.Generic;

namespace SharpBank.Services
{
    public class BankService : IBankService
    {
        private List<CustomerService> customers;

        public BankService()
        {
            customers = new List<CustomerService>();
        }

        public void AddCustomer(CustomerService customer)
        {
            customers.Add(customer);
        }

        public string CustomerSummary()
        {
            string summary = "Customer Summary";
            foreach (CustomerService customer in customers)
                summary += "\n - " + customer.GetName() + " (" + Format(customer.GetNumberOfAccounts(), "account") + ")";
            return summary;
        }

        //Make sure correct plural of word is created based on the number passed in:
        //If number passed in is 1 just return the word otherwise add an 's' at the end
        private string Format(int number, string word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }

        public double TotalInterestPaid()
        {
            double total = 0;
            foreach (CustomerService customer in customers)
                total += customer.GetTotalInterestEarned();
            return total;
        }

        public string GetFirstCustomer() //LINQ - customers.FirstOrDefault()
        {
            try
            {
                return customers?[0].GetName();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "Error";
            }
        }
    }
}
