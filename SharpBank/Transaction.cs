﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double amount;
        public readonly DateTime transactionDate;

        public Transaction(double amount)
        {
            this.amount = amount;
            this.transactionDate = DateProvider.GetInstance().Now();
        }

    }
}
